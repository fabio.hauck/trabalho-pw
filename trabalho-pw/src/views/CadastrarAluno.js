
import 'bootstrap/dist/css/bootstrap.min.css';
import apiService from '../Api';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';
function CadastrarAluno() {

	const [nome, setNome] = useState("");
	const [email, setEmail] = useState("");
	const [alunos, setAlunos] = useState({});
	let navigate = useNavigate();


	useEffect(() => { 
		setAlunos({});
	},[]);

	const clickBotao = () =>
	{
		apiService.listAlunos().then((response) => {
			
			apiService.inserirAluno(response.length + 1, nome, email)});
		
		navigate('/');
	}


  return (
	<div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
	<div className="d-table-cell align-middle">

		<div className="text-center mt-4">
			<h1 className="h2">Cadastrando Aluno</h1>
			<p className="lead">
				Informe as informações do aluno que será cadastrado!
			</p>
		</div>

		<div className="card bg-secondary">
			<div className="card-body">
				<div className="m-sm-4">
					<form>
						<div className="mb-3">
							<label className="form-label">Nome</label>
							<input className="form-control form-control-lg" type="text" name="name" onChange={(e) => setNome(e.target.value)} placeholder="Enter your name" />
						</div>
						<div className="mb-3">
							<label className="form-label">Email</label>
							<input className="form-control form-control-lg" type="email" name="email" onChange={(e) => setEmail(e.target.value)} placeholder="Enter your email"/>
						</div>
					
						<div className="text-center mt-3">
							<button className="btn btn-lg btn-primary" type="submit" onClick={() => clickBotao()}>Cadastrar</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
  );
}

export default CadastrarAluno;
