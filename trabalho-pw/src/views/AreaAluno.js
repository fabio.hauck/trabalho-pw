
import 'bootstrap/dist/css/bootstrap.min.css';
import apiService from '../Api';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';


function AreaAluno() {

	const [nome, setNome] = useState("");
	const [email, setEmail] = useState("");
	const [descricao, setDescricao] = useState("");
	const [duracao, setDuracao] = useState(0);
	const [aluno, setAluno] = useState({});
	const [fichas, setFichas] = useState([]);
	const [listar, setListar] = useState(false);
	const [idFichas, setIdFichas] = useState([]);
	let navigate = useNavigate();


	useEffect(() => { 
		setAluno({});
		setIdFichas({});
		setFichas({});
	},[]);


	const clickBotao = (e) =>
	{

		apiService.listAlunos().then((response) => {
		 console.log(response)
			 response.map(function(r){
				if(r.email == e)
				{
					setEmail(e);
					let array = [];
					console.log("igual!");
					console.log(r) 
					setAluno(r);
					apiService.findAlunoFichas(r).then((response) => {
						console.log(response);
						var ids = response.split(",");
						setIdFichas(ids);
						ids.map(function(r){
							apiService.findFicha(r).then((response) => {
								array.push(response);
							})
						});
						console.log(array);
						setFichas(array);
					})
					apiService.listExercicios().then((r) => {
						console.log(r);
					})
					setAluno(r);
				}
			}); 		
			setListar(true);
		});
	}
	
	const clickBotaoEditar = (e) =>
	{

		
          

	 apiService.editarAluno(aluno,nome,email).then((response) => {
		console.log(response);		
		setAluno(response.data);

		});
		
	}
	
	const clickBotaoEditarFicha = (e) =>
	{

		console.log(e.target.id);
		apiService.findFicha(e.target.id).then((response) => {
			console.log(response);		
			console.log(descricao);		
			console.log(duracao);		
		
			apiService.editarFicha(response,descricao,duracao).then((response) => {
				console.log(response);		
				setAluno(response.data);
		
				}); 
			}); 
		
		
	}

	const clickBotaoFicha = (e) =>
	{
		
		apiService.inserirFicha(aluno,descricao,duracao).then((response) => {
			

			apiService.atualizarFichaAluno(aluno,response.data.id).then((response) => {
				console.log(response);
			});
		 
	});
	console.log(aluno) 
	setDescricao("");
	setDuracao("");
	}

	 const Listar = (e) =>
	 {
		 return (<>
		<table className='table table-bordered table-hover'>
			
			<thead>
				<tr>
				<th>Id</th>
				<th>Descrição</th>
				<th>Duracao</th>
				<th>Ações</th>
				
				</tr>
				
			</thead>
			<tbody>
			{e.fichas.length > 0 && e.fichas.map((r) => {
			return(
			<>
			<tr>
			<td>{r.id}</td>
			<td>{r.descricao}</td>
			<td>{r.duracao}</td>
			<td><a key={r.id} id={r.id} className="btn btn-lg btn-primary m-1" href="/area-aluno" onClick={(e ) => clickBotaoEditarFicha(e)} >Editar Ficha</a><button key={r.id} className="btn btn-lg btn-primary m-1" >Ver Exercicios</button><br></br><button key={r.id} className="btn btn-lg btn-primary m-1" >Incluir Exercicio</button><a key={r.id} className="btn btn-lg btn-primary m-1" href='/' onClick={(e) => { 	apiService.deleteFicha(r.id).then((response) => {console.log(response)})}}>Excluir Ficha</a></td>
			
			</tr>
			</>
			)
		})}
		
			</tbody>
		</table>
		</>)
		
		
	}
 

  return (
	  <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
		<div className="d-table-cell align-middle">
			<a  className="btn btn-lg btn-primary mt-3 mb-3 " type="submit" href='/'>Voltar</a>

	  { (aluno.nome === undefined) &&
		<div className="text-center mt-4">
			<h1 className="h2">Area Aluno</h1>
			<h4 className="lead">
				Informe o email para acessar a sua ficha!
			</h4>
		</div>}
		{ (aluno.nome === undefined) &&
		<div className="card bg-secondary">
			<div className="card-body">
				<div className="m-sm-4">
					<form>
						<div className="mb-3">
							<h3 className="form-label">Email</h3>
							<input className="form-control form-control-lg" type="email" name="email" onChange={(e) => clickBotao(e.target.value)} placeholder="Enter your email"/>
							<label className='text-danger justify-content-center'>Aluno não encontrado pelo email informado...</label>
						</div>
						
					</form>
				</div>
			</div>
		</div>
		}
	
		{ (aluno.nome !== undefined) &&
		<div>
			
			
			<div className="card bg-secondary">
			<div className="card-body">
				<div className="m-sm-4">
				
								<div className="text-center mt-4">
									<h1 className="h2">Olá {aluno.nome}</h1>
									<h5>
										Informações do aluno:
									</h5>

					
								<div className="m-sm-4">
									
										<div className="mb-3">
											<label className="form-label">Nome:</label>
											<input className="form-control form-control-lg" type="text" defaultValue={aluno.nome} onChange={(e) => setNome(e.target.value)}  name="name" placeholder="Enter your name"/>
										</div>
						
										<div className="mb-3">
											<label className="form-label">Email:</label>
											<input className="form-control form-control-lg" type="email" defaultValue={aluno.email} onChange={(e) => setEmail(e.target.value)} name="email" placeholder="Enter your email"/>
										</div>
										<button className="btn btn-lg btn-primary" onClick={() => clickBotaoEditar()}>Editar</button>
										<hr></hr>
										<h4 className='mt-3'>
										Cadastrar Fichas:
										</h4>
										
										<div className="mb-3">
											<label className="form-label">Descrição:</label>
											<textarea className="form-control form-control-lg" type="text" onLoad={(e) => {console.log(e)} } onChange={(e) => setDescricao(e.target.value)} name="name" value={descricao} placeholder="Enter your name"/>
										</div>
						
										<div className="mb-3">
											<label className="form-label">Duração:</label>
											<input className="form-control form-control-lg justify-content-center text-center" type="time" value={duracao} onChange={(e) => setDuracao(e.target.value)} name="time" />
										</div>
										<div className='row'>
										<div className='col-2'/>
										<div className='col-2'/>
										<button className="btn btn-lg btn-primary col-4" type="submit" onClick={() => clickBotaoFicha()}>Cadastrar Ficha</button>
										<div className='col-1'/>
										</div>

								</div>
								</div>
				<Listar fichas={fichas}/>
				</div>
			</div>
		
		</div>
		
		
		
			
		</div>}
		
	
												
		

	</div>
</div>
  );
}

export default AreaAluno;
