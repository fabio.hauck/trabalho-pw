import axios from "axios";

const apiService = axios.create({
    baseURL: "http://localhost:3000",
});

apiService.interceptors.request.use(async config => {
    config.headers = {
        Accept: 'application/json',
        ContentType: 'application/json',
    };
    return config;
});

const listAlunos = () => {
    return apiService.get("/alunos").then((response) => {
        return response.data;
    });
}


const findFicha = ( id ) => {

    return apiService.get("/fichas/"+id).then((response) => {
        return response.data;
    });
}

const deleteFicha = ( id ) => {

    return apiService.delete("/fichas/"+id).then((response) => {
        return response.data;
    });
}

const listFichas = () => {
    return apiService.get("/fichas").then((response) => {
        return response.data;
    });
}

const listExercicios = () => {
    return apiService.get("/exercicios").then((response) => {
        return response.data;
    });
}

const findAluno = (id) => {
    return apiService.get("/alunos/" + id).then((response) => {
        return response;
    });
}

const deleteAluno = (id) => {
    return apiService.delete("/alunos/" + id).then((response) => {
        return response;
    });
}
const findAlunoFichas = (aluno) => {
    return apiService.get("/alunos/" + aluno.id).then((response) => {
        var ids = "";
        response.data.fichas.map(function(r) {
            ids += r;
        })
        console.log(ids)
        return ids;
    });
}


const inserirAluno = (id, nome, email) => {
    
    const json = {
        
        "id": id,
        "nome": nome,
        "email": email,
        "fichas": []
    }
    
    return apiService.post("/alunos", json).then((response) => {
        
        return response;
    });
}

const atualizarFichaAluno = (aluno, id) => {
    
    listFichas().then((r) => {
        console.log(r);
        var ids = "";
        var count = 1;
        r.map(function(response){
            if(response.idAluno == aluno.id)
            {
                if(count == 1)
                {
                    ids += response.id;
                    count++;

                }
                else{
                    ids += ","+response.id;
                    console.log(r);

                }

            }
          });
          return apiService.put("/alunos/"+aluno.id, {
              ...aluno,
              "fichas" : [ids]
          }).then((response) => {
              return response;
          });
    });
    
       
    
}


        const inserirFicha = (aluno, descricao, duracao) => {


             return apiService.post("/fichas", {
                            "idAluno" : aluno.id,
                            "descricao": descricao,
                            "duracao": duracao,
                            "exercicios": []

                    
                    }
                ).then((response) => {

                    return response;
                }); 
        } 

        const editarAluno = (aluno, nome, email) => {
            if (nome === undefined || nome === "" || nome == null) {
                nome = aluno.nome;
            }
            if (email === undefined || email === "" || email == null) {
                email = aluno.email;
            }



            return apiService.put("/alunos/"+aluno.id, {
                ...aluno,
                "id": aluno.id,
                "nome": nome,
                "email": email,
            }).then((response) => {
                return response;
            });
        }

        const editarFicha = (ficha, descricao, duracao) => {
            if (descricao === undefined || descricao === "" || descricao == null) {
                descricao = ficha.descricao;
            }
            if (duracao === undefined || duracao === "" || duracao == null) {
                duracao = ficha.duracao;
            }



            return apiService.put("/fichas/"+ficha.id, {
                ...ficha,
                "descricao": descricao,
                "duracao": duracao,
              
            }).then((response) => {
                return response;
            });
        }


        export default {
            apiService,
            listAlunos,
            listFichas,
            listExercicios,
            findAluno,
            inserirAluno,
            editarAluno,
            inserirFicha,
            findFicha,
            findAlunoFichas,
            atualizarFichaAluno,
            editarFicha,
            deleteFicha,
            deleteAluno

        };