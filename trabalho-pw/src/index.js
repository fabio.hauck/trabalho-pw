import {React} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import CadastrarAluno from './views/CadastrarAluno';
import AreaAluno from './views/AreaAluno';
import { BrowserRouter, Route, Routes } from "react-router-dom"; 
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';

function PrivateRoute ({ ...rest })
{
  return (
    <>
      <Routes>
        <>
          <Route path="/" element={<App/>} /> 
          <Route path="cadastrar-aluno" element={<CadastrarAluno />} /> 
          <Route path="area-aluno" element={<AreaAluno />} /> 
        </>
      </Routes>
    </>
  )
}

ReactDOM.render(
<>
  <BrowserRouter>
    <div className="wrapper">
        <div className="main">
          < PrivateRoute />
        </div>
    </div>
  </BrowserRouter>
</>,
document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
