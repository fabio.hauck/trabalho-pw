
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from 'react-router-dom';
function App() {

	let navigate = useNavigate();

  return (
    <div className="App justify-content-center ">
	
   <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100 ">
					<div className="d-table-cell align-middle">

						<div className="text-center mt-4">
							<h1 className="h2">Academia Vianna Junior</h1>
							<p className="lead">
								App feito pra você e melhorando sua experiência em malhar com a gente.
							</p>
						</div>

						<div className="card bg-dark">
							<div className="card-body">
								<div className="m-sm-4">
									<form>
										<div className="mb-3">
											<div>
											<h3 className='text-white'>Já tenho conta cadastrada, acessar!</h3>
											</div>
									
										</div>
									
										<div className="text-center mt-3">
											<button className="btn btn-lg btn-primary" onClick={() => { navigate('/area-aluno') }}>Acessar</button>
										</div>
									</form>
								</div>
							</div>
						</div>

            <div className="card bg-warning mt-3">
							<div className="card-body">
								<div className="m-sm-4">
									<form>
										<div className="mb-3">
										<div>
                    <h3 className='text-dark'>Não tenho conta cadastrada, cadastrar!</h3>
                    </div>
											</div>
									
										<div className="text-center mt-3">
											<a className="btn btn-lg btn-primary" href="/cadastrar-aluno">Cadastrar</a> 
										</div>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
    </div>
  );
}

export default App;
